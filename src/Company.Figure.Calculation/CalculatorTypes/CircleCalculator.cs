﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Figure.Calculation
{
    internal class CircleCalculator : IFigureCalculator
    {
        private double _radius;

        public CircleCalculator(double[] sides)
        {
            IsValid(sides);
            _radius = sides[0];
        }

        public void IsValid(double[] sides)
        {
            if (sides.Count() != 1)
                throw new ArgumentException($"Было указано измерений: {sides.Count()}, вместо необходимых 1-го (радиуса).");
            if (sides[0] <= 0)
                throw new ArgumentException("Радиус окружности должен быть положительным числом.");
        }

        public double GetArea()
        {
            return Math.Round(Math.PI * _radius * _radius, 2);
        }
    }
}
