﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Figure.Calculation
{
    internal class SquareCalculator : IFigureCalculator
    {
        private double _side;

        public SquareCalculator(double[] sides)
        {
            IsValid(sides);
            _side = sides[0];
        }

        public void IsValid(double[] sides)
        {
            if (sides.Count() != 1)
                throw new ArgumentException($"Было указано измерений: {sides.Count()}, вместо необходимых 1-го (стороны квадрата).");
            if (sides[0] <= 0)
                throw new ArgumentException("Сторона квадрата должна быть положительным числом.");
        }

        public double GetArea()
        {
            return Math.Round(_side * _side, 2);
        }
    }
}