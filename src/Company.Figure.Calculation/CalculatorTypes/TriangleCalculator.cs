﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Figure.Calculation
{
    internal class TriangleCalculator : IFigureCalculator
    {
        private double _sideA;
        private double _sideB;
        private double _sideC;

        public TriangleCalculator(double[] sides)
        {
            IsValid(sides);
            _sideA = sides[0];
            _sideB = sides[1];
            _sideC = sides[2];
        }

        public void IsValid(double[] sides)
        {
            if (sides.Count() != 3)
                throw new ArgumentException($"Было указано измерений: {sides.Count()}, вместо необходимых 3-ёх (сторон треугольника).");
            if (sides[0] <= 0 || sides[1] <= 0 || sides[2] <= 0)
                throw new ArgumentException("Все стороны треугольника должны быть положительным числом.");
            if (sides[0] + sides[1] <= sides[2] || sides[0] + sides[2] <= sides[1] || sides[2] + sides[1] <= sides[0])
                throw new Exception($"Треугольник со сторонами {sides[0]}, {sides[1]}, {sides[2]} не существует.");
        }

        /// <summary>
        /// Вычисляет площадь треугольника через его полупериметр.
        /// </summary>
        public double GetArea()
        {
            double p = (_sideA + _sideB + _sideC) / 2;
            return Math.Round(Math.Sqrt(p * (p - _sideA) * (p - _sideB) * (p - _sideC)), 2);
        }

        /// <summary>
        /// Проверяет является ли треугольник прямоугольным через теорему Пифагора.
        /// </summary>
        public bool IsRight()
        {
            double leg1; //предполагаемый катет 1
            double leg2; //предполагаемый катет 2
            double hypotenuse; //предполагаемая гипотенуза (самая длинная сторона)

            if (_sideA >= _sideB && _sideA >= _sideC)
            {
                hypotenuse = _sideA;
                leg1 = _sideB;
                leg2 = _sideC;
            }
            else
                if (_sideB >= _sideA && _sideB >= _sideC)
            {
                hypotenuse = _sideB;
                leg1 = _sideA;
                leg2 = _sideC;
            }
            else
            {
                hypotenuse = _sideC;
                leg1 = _sideA;
                leg2 = _sideB;
            }
            if (hypotenuse * hypotenuse == leg1 * leg1 + leg2 * leg2)
                return true;
            else
                return false;
        }
    }
}