﻿namespace Company.Figure.Calculation
{
    public class Figure
    {
        dynamic FigureType { get; }

        /// <summary>
        /// Создает фигуру для математических вычислений.
        /// </summary>
        /// <param name="shape">Тип фигуры в формате строки. Доступны фигуры: треугольник, окружность, квадрат.</param>
        /// <param name="value">Измерения фигуры в формате массива вещественных чисел двойной точности с плавающей запятой (double[]).</param>
        public Figure(string shape, double[] value)
        {
            switch (shape.ToLower())
            {
                case "треугольник":
                    FigureType = new TriangleCalculator(value);
                    break;
                case "квадрат":
                    FigureType = new SquareCalculator(value);
                    break;
                case "окружность":
                    FigureType = new CircleCalculator(value);
                    break;
                default:
                    throw new ArgumentException("Тип фигуры указан неверно. Доступны фигуры \"треугольник\", \"квадрат\" и \"окружность\".");
                    break;
            }
        }

        /// <summary>
        /// Вычисляет площадь фигуры и округляет ее до 2-ух знаков после запятой.
        /// </summary>
        public double GetArea()
        {
            return FigureType.GetArea();
        }

        /// <summary>
        /// Проверяет является ли треугольник прямоугольным.
        /// </summary>
        public bool CheckTrianleIsRight()
        {
            if (FigureType.GetType().Name == "TriangleCalculator")
            {
                return FigureType.IsRight();
            }
            else
                throw new Exception("Тип фигуры не является треугольником для проверки \"Является ли треугольник прямоугольным\".");
        }
    }
}