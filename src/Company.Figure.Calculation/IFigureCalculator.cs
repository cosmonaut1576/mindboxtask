﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.Figure.Calculation
{
    internal interface IFigureCalculator
    {
        void IsValid(double[] sides);
        double GetArea();
    }
}
