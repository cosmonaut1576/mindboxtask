using Company.Figure.Calculation;

namespace TestProject1
{
    public class UnitTests
    {
        [Test]
        public void TestGetAreaForCircle()
        {
            double[] radius = new double[] { 3 };

            Figure figure = new Figure("����������", radius);

            double expectedResult = 28.27;
            
            double actualResult = figure.GetArea();

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void TestGetAreaForTriangle()
        {
            double[] sides = new double[] { 3, 4, 5 };

            Figure figure = new Figure("�����������", sides);

            double expectedResult = 6;

            double actualResult = figure.GetArea();

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void TestGetAreaForSquare()
        {
            double[] side = new double[] { 3 };
            Figure figure = new Figure("�������", side);

            double expectedResult = 9;

            double actualResult = figure.GetArea();

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void TestCreateFigureWithWrongShapeInput()
        {
            try
            {
                double[] sides = new double[] { 3, 4};

                Figure figure = new Figure("�������������", sides);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message == "��� ������ ������ �������. �������� ������ \"�����������\", \"�������\" � \"����������\".")
                    return;
                else
                    Assert.Fail();
            }
            Assert.Fail();

        }

        [Test]
        public void TestNegativeInputForCircle()
        {
            try
            {
                double[] radius = new double[] { -3 };

                Figure figure = new Figure("����������", radius);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message == "������ ���������� ������ ���� ������������� ������.")
                    return;
                else
                    Assert.Fail();
            }
            Assert.Fail();
        }

        [Test]
        public void TestNegativeInputForTriangle()
        {
            try
            {
                double[] sides = new double[] { -3, 4, 5 };

                Figure figure = new Figure("�����������", sides);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message == "��� ������� ������������ ������ ���� ������������� ������.")
                    return;
                else
                    Assert.Fail();
            }
            Assert.Fail();
        }

        [Test]
        public void TestNegativeInputForSquare()
        {
            try
            {
                double[] side = new double[] { -3 };
                Figure figure = new Figure("�������", side);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message == "������� �������� ������ ���� ������������� ������.")
                    return;
                else
                    Assert.Fail();
            }
            Assert.Fail();
        }

        [Test]
        public void TestWrongSidesCountInputForCircle()
        {
            try
            {
                double[] radius = new double[0];

                Figure figure = new Figure("����������", radius);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("���� ������� ���������:"))
                    return;
                else
                    Assert.Fail();
            }
            Assert.Fail();
        }

        [Test]
        public void TestWrongSidesCountInputForTriangle()
        {
            try
            {
                double[] radius = new double[1] { 1 };

                Figure figure = new Figure("�����������", radius);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("���� ������� ���������:"))
                    return;
                else
                    Assert.Fail();
            }
            Assert.Fail();
        }

        [Test]
        public void TestWrongSidesCountInputForSquare()
        {
            try
            {
                double[] radius = new double[0];

                Figure figure = new Figure("�������", radius);
            }
            catch (ArgumentException ex)
            {
                if (ex.Message.StartsWith("���� ������� ���������:"))
                    return;
                else
                    Assert.Fail();
            }
            Assert.Fail();
        }

        [Test]
        public void TestTriangleNotExists()
        {
            try
            {
                double[] sides = new double[] { 10, 4, 5 };

                Figure figure = new Figure("�����������", sides);
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("����������� �� ���������"))
                    return;
                else
                    Assert.Fail();
            }
            Assert.Fail();
        }

        [Test]
        public void TestCheckTriangleIsRightWithTrueResult()
        {
            double[] sides = new double[] { 3, 4, 5 };

            Figure figure = new Figure("�����������", sides);

            bool expectedResult = true;

            bool actualResult = figure.CheckTrianleIsRight();

            Assert.AreEqual(expectedResult, actualResult);
        }


        [Test]
        public void TestCheckTriangleIsRightWithFalseResult()
        {
            double[] sides = new double[] { 4, 5, 6 };

            Figure figure = new Figure("�����������", sides);

            bool expectedResult = false;

            bool actualResult = figure.CheckTrianleIsRight();

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void TestCheckTriangleIsRightOnWrongShape()
        {
            try
            {
                double[] sides = new double[] { 10 };

                Figure figure = new Figure("�������", sides);

                figure.CheckTrianleIsRight();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("��� ������ �� �������� ������������� ��� ��������"))
                    return;
                else
                    Assert.Fail();
            }
            Assert.Fail();
        }
    }
}