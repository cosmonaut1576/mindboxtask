﻿using Company.Figure.Calculation;

class Program
{
    private static void Main(string[] args)
    {
        Figure figure = new Figure("окружность", new double[] { 3 });
        Console.WriteLine(figure.GetArea());

        figure = new Figure("треугольник", new double[] { 3, 4, 5 });
        Console.WriteLine(figure.GetArea());
        Console.WriteLine(figure.CheckTrianleIsRight());

        figure = new Figure("треугольник", new double[] { 6, 7, 8 });
        Console.WriteLine(figure.GetArea());
        Console.WriteLine(figure.CheckTrianleIsRight());
    }
}